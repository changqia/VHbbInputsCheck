from ROOT import *

eos_dir = '/eos/atlas/atlascerngroupdisk/phys-higgs/HSG5/Run2/ICHEP2016/'
#inputs= eos_dir+'LAL/LimitHistograms.VH.vvbb.13TeV.LAL.v22.root' # 0lep
#inputs1= eos_dir+'UCL/LimitHistograms.VH.lvbb.13TeV.UCL.v14.PP8MJ.namefix.root' # 1lep
#inputs2= eos_dir+'UCL/LimitHistograms.VH.lvbb.13TeV.UCL.v15.root' # 1lep
inputs1= eos_dir+'LiverpoolBmham/LimitHistograms.VH.llbb.13TeV.LiverpoolBmham.v54.root' #2lep
inputs2= eos_dir+'LiverpoolBmham/LimitHistograms.VH.llbb.13TeV.LiverpoolBmham.v55.root' #2lep

# if you don't want to show the syst. passing check, turn it off
ShowSucess = True

Processes = [
'data',
# W+jets
'Wbb','Wbc','Wbl','Wcc','Wcl','Wl',
# Z+jets
'Zbb','Zbc','Zbl','Zcc','Zcl','Zl',
# signal
# 1 lepton
#'qqWlvH125',
'qqZllH125',
#0 lep: 'qqZvvH125','ggZvvH125',
# diboson
'WW','WZ','ZZ',
# top
'ttbar','stopWt','stops','stopt',
]

Vars = [
'mva',
#'mva28', # 0 lep
#'mBB',
]

Regions = [
# 0lep:
#'2tag2jet_150ptv_SR',
#'2tag3jet_150ptv_SR',

# 1lep:
#'2tag2jet_150ptv_WhfCR',
#'2tag3jet_150ptv_WhfCR',
#'2tag2jet_150ptv_WhfSR',
#'2tag3jet_150ptv_WhfSR',

## 2lep:
'2tag2jet_0_75ptv_SR',
'2tag2jet_75_150ptv_SR',
'2tag2jet_150ptv_SR',
'2tag3jet_0_75ptv_SR',
'2tag3jet_75_150ptv_SR',
'2tag3jet_150ptv_SR',
#
## 2lep CR, please check mBBMVA instead of mva
#'2tag2jet_0_75ptv_topemucr',
#'2tag2jet_75_150ptv_topemucr',
#'2tag2jet_150ptv_topemucr',
#'2tag3jet_0_75ptv_topemucr',
#'2tag3jet_75_150ptv_topemucr',
#'2tag3jet_150ptv_topemucr',

]

# Booking the systematics
# 'Name' and 'OneSide' must be in place
# 'Region' and 'Process' when needed
# if 'Process' set, means only process in the list checked, same for 'Region'

Systs = [
#{'Name':'blablabla','OneSide':False,'Region':[],'Process':[]},
# two side general sys
{'Name':'SysEG_RESOLUTION_ALL','OneSide':False},
{'Name':'SysEG_SCALE_ALL','OneSide':False},
{'Name':'SysEL_EFF_ID_TOTAL_1NPCOR_PLUS_UNCOR','OneSide':False},
{'Name':'SysEL_EFF_Iso_TOTAL_1NPCOR_PLUS_UNCOR','OneSide':False},
{'Name':'SysEL_EFF_Reco_TOTAL_1NPCOR_PLUS_UNCOR','OneSide':False},
{'Name':'SysEL_EFF_Trigger_TOTAL_1NPCOR_PLUS_UNCOR','OneSide':False},
{'Name':'SysFT_EFF_Eigen_B_0_AntiKt4EMTopoJets','OneSide':False},
{'Name':'SysFT_EFF_Eigen_B_1_AntiKt4EMTopoJets','OneSide':False},
{'Name':'SysFT_EFF_Eigen_B_2_AntiKt4EMTopoJets','OneSide':False},
{'Name':'SysFT_EFF_Eigen_C_0_AntiKt4EMTopoJets','OneSide':False},
{'Name':'SysFT_EFF_Eigen_C_1_AntiKt4EMTopoJets','OneSide':False},
{'Name':'SysFT_EFF_Eigen_C_2_AntiKt4EMTopoJets','OneSide':False},
{'Name':'SysFT_EFF_Eigen_C_3_AntiKt4EMTopoJets','OneSide':False},
{'Name':'SysFT_EFF_Eigen_Light_0_AntiKt4EMTopoJets','OneSide':False},
{'Name':'SysFT_EFF_Eigen_Light_1_AntiKt4EMTopoJets','OneSide':False},
{'Name':'SysFT_EFF_Eigen_Light_2_AntiKt4EMTopoJets','OneSide':False},
{'Name':'SysFT_EFF_Eigen_Light_3_AntiKt4EMTopoJets','OneSide':False},
{'Name':'SysFT_EFF_Eigen_Light_4_AntiKt4EMTopoJets','OneSide':False},
{'Name':'SysFT_EFF_extrapolation_AntiKt4EMTopoJets','OneSide':False},
{'Name':'SysFT_EFF_extrapolation_from_charm_AntiKt4EMTopoJets','OneSide':False},
{'Name':'SysJET_21NP_JET_BJES_Response','OneSide':False},
{'Name':'SysJET_21NP_JET_EffectiveNP_1','OneSide':False},
{'Name':'SysJET_21NP_JET_EffectiveNP_2','OneSide':False},
{'Name':'SysJET_21NP_JET_EffectiveNP_3','OneSide':False},
{'Name':'SysJET_21NP_JET_EffectiveNP_4','OneSide':False},
{'Name':'SysJET_21NP_JET_EffectiveNP_5','OneSide':False},
{'Name':'SysJET_21NP_JET_EffectiveNP_6','OneSide':False},
{'Name':'SysJET_21NP_JET_EffectiveNP_7','OneSide':False},
{'Name':'SysJET_21NP_JET_EffectiveNP_8restTerm','OneSide':False},
{'Name':'SysJET_21NP_JET_EtaIntercalibration_Modelling','OneSide':False},
{'Name':'SysJET_21NP_JET_EtaIntercalibration_NonClosure','OneSide':False},
{'Name':'SysJET_21NP_JET_EtaIntercalibration_TotalStat','OneSide':False},
{'Name':'SysJET_21NP_JET_Flavor_Composition','OneSide':False},
{'Name':'SysJET_21NP_JET_Flavor_Response','OneSide':False},
{'Name':'SysJET_21NP_JET_Pileup_OffsetMu','OneSide':False},
{'Name':'SysJET_21NP_JET_Pileup_OffsetNPV','OneSide':False},
{'Name':'SysJET_21NP_JET_Pileup_PtTerm','OneSide':False},
{'Name':'SysJET_21NP_JET_Pileup_RhoTopology','OneSide':False},
{'Name':'SysJET_21NP_JET_PunchThrough_MC15','OneSide':False},
{'Name':'SysJET_21NP_JET_SingleParticle_HighPt','OneSide':False},
{'Name':'SysJET_JvtEfficiency','OneSide':False},
#{'Name':'SysJET_SR1_JET_GroupedNP_1','OneSide':False}, # the reduced set of NPs for JES which we don't use
#{'Name':'SysJET_SR1_JET_GroupedNP_2','OneSide':False},
#{'Name':'SysJET_SR1_JET_GroupedNP_3','OneSide':False},
{'Name':'SysMET_JetTrk_Scale','OneSide':False},
{'Name':'SysMET_SoftTrk_Scale','OneSide':False},
{'Name':'SysMETTrigStat','OneSide':False},
{'Name':'SysMETTrigTop','OneSide':False},
{'Name':'SysMUON_EFF_STAT','OneSide':False},
{'Name':'SysMUON_EFF_STAT_LOWPT','OneSide':False},
{'Name':'SysMUON_EFF_SYS','OneSide':False},
{'Name':'SysMUON_EFF_SYS_LOWPT','OneSide':False},
{'Name':'SysMUON_EFF_TrigStatUncertainty','OneSide':False},
{'Name':'SysMUON_EFF_TrigSystUncertainty','OneSide':False},
{'Name':'SysMUON_ID','OneSide':False},
{'Name':'SysMUON_ISO_STAT','OneSide':False},
{'Name':'SysMUON_ISO_SYS','OneSide':False},
{'Name':'SysMUON_MS','OneSide':False},
{'Name':'SysMUON_SAGITTA_RESBIAS','OneSide':False},
{'Name':'SysMUON_SAGITTA_RHO','OneSide':False},
{'Name':'SysMUON_SCALE','OneSide':False},
{'Name':'SysMUON_TTVA_STAT','OneSide':False},
{'Name':'SysMUON_TTVA_SYS','OneSide':False},
{'Name':'SysPRW_DATASF','OneSide':False},

# one side booked here
{'Name':'SysJET_JER_SINGLE_NP','OneSide':True},
{'Name':'SysMET_SoftTrk_ResoPara','OneSide':True},
{'Name':'SysMET_SoftTrk_ResoPerp','OneSide':True},

# some special syst.
{'Name':'SysTTbarMBB','OneSide':False,'Process':['ttbar']},
{'Name':'SysTTbarPTV','OneSide':False,'Process':['ttbar']},
{'Name':'SysTTbarPTVMBB','OneSide':False,'Process':['ttbar']},

{'Name':'SysStopWtMBB','OneSide':False,'Process':['stopWt']},
{'Name':'SysStopWtPTV','OneSide':False,'Process':['stopWt']},

{'Name':'SysWMbb','OneSide':False,'Process':['Wbb','Wbc','Wbl','Wcc','Wcl']},
{'Name':'SysWPtV','OneSide':False,'Process':['Wbb','Wbc','Wbl','Wcc','Wcl']},
{'Name':'SysWPtVMbb','OneSide':False,'Process':['Wbb','Wbc','Wbl','Wcc','Wcl']},

{'Name':'SysZMbb','OneSide':False,'Process':['Zbb','Zbc','Zbl','Zcc','Zcl']},
{'Name':'SysZPtV','OneSide':False,'Process':['Zbb','Zbc','Zbl','Zcc','Zcl']},
{'Name':'SysZPtVMbb','OneSide':False,'Process':['Zbb','Zbc','Zbl','Zcc','Zcl']},

]

class bcolors:
    HEADER = '\033[95m'
    OKBLUE = '\033[94m'
    OKGREEN = '\033[92m'
    WARNING = '\033[93m'
    FAIL = '\033[91m'
    ENDC = '\033[0m'

    def disable(self):
        self.HEADER = ''
        self.OKBLUE = ''
        self.OKGREEN = ''
        self.WARNING = ''
        self.FAIL = ''
        self.ENDC = ''

def Error(info):
  print bcolors.FAIL + info + bcolors.ENDC

def Sucess(info):
  if ShowSucess:
    print bcolors.OKGREEN + info + bcolors.ENDC

def Check(name, f1, f2):
  exist1 = f1.GetListOfKeys().Contains(name)
  exist2 = f2.GetListOfKeys().Contains(name)
  if ~exist1 and ~exist2 and "AntiKt4EMTopoJets" in name:
    name = name.replace("_AntiKt4EMTopoJets", "");
    exist1 = f1.GetListOfKeys().Contains(name)
    exist2 = f2.GetListOfKeys().Contains(name)

  entry1 = -1.
  entry2 = -1.
  #pass by reference
  error1 = Double(-1.)
  error2 = Double(-1.)
  if exist1:
    h1 = f1.Get(name)
    #entry1 = h1.Integral()
    nbin=h1.GetNbinsX()
    entry1 = h1.IntegralAndError(0, nbin, error1)
  if exist2:
    h2 = f2.Get(name)
    #entry2 = h2.Integral()
    nbin=h2.GetNbinsX()
    entry2 = h2.IntegralAndError(0, nbin, error2)

  info="Missing"
  if exist1 and exist2:
    dif = 1.0
    if entry1 > 0. :
      dif = (entry2 - entry1) * 100 / entry1
    comment="OK"
    absdif = abs(dif)
    if (absdif > 5.) & (absdif <= 10.): 
      comment="SMALL"
    elif (absdif > 10.) & (absdif <= 50.): 
      comment="MEDIUM"
    elif (absdif > 50.) & (absdif <= 100.): 
      comment="LARGE"
    elif (absdif > 100.): 
      comment="HUGE"

    info = "{:+4.1f}\%, {}".format(dif, comment)

  #name.replace("_AntiKt4EMTopoJets", "")
  print "{:90s}: {:9.2f} +- {:6.2f}, {:9.2f} +- {:6.2f}; ({})".format(name, entry1, error1, entry2, error2, info)


def CheckNominal(f1, f2):
  for reg in Regions:
    for pro in Processes:
      for var in Vars:
        histoname = pro+'_'+reg+'_'+var
        Check(histoname, f1, f2)

def CheckSyst(f1, f2):
  for sys in Systs:
    SysName = sys['Name']
    OneSide = sys['OneSide']
    #print
    for reg in Regions:
      if 'Region' in sys and reg not in sys['Region']:
        continue
      for pro in Processes:
        if 'Process' in sys and pro not in sys['Process']:
          continue
        if pro is 'data':
          continue
        for var in Vars:
          histoname = pro+'_'+reg+'_'+var+'_'+SysName
          Check(histoname+'__1up', f1, f2)
          if not OneSide:
            Check(histoname+'__1down', f1, f2)

# start check
print 'input1: {}'.format(inputs1)
print 'input2: {}'.format(inputs2)
print '\n'
f1 = TFile(inputs1,'READ');
f2 = TFile(inputs2,'READ');
CheckNominal(f1, f2)
d1 = f1.GetDirectory('Systematics');
d2 = f2.GetDirectory('Systematics');
CheckSyst(d1, d2)
